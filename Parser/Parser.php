<?php

/*
 * 
 * Data that output form filesystem are parsed here. Also parameter string for twitter api url is composed here
 * 
 */

class Parser {
    
    protected $parsedString;
    
    protected $request;
    
    public function setRequest($request) {
        if($request) {
            $this->request = $request;
        }
    }
    
    public  function parseFileOutput($data) {
        $data = str_replace("\r", "", $data);
        $data = str_replace("\n", "", $data);
        
        $this->parsedString = $data;
    }
    
    public function composeGetString() {
        if(isset($this->request)) {
            $this->parsedString = "?screen_name=" . $this->request['screen_name'] . "&skip_status=1&count=200";
        }  else {
            $filename = $_SERVER['DOCUMENT_ROOT'] . "/TwitterFlowers/list/ToSearch.txt";
            if(file_exists($filename)) {
                $fh = fopen($filename, "r");
                if($fh) {
                    $toSearch = fgets($fh);
                    $this->parseFileOutput($toSearch);
                    $this->parsedString = "?screen_name=" . $this->parsedString . "&count=200";
                }
            }
        }
    }
    
    public function getResult() {
        return $this->parsedString;
    }
    
    public function GetScreenName() {
        if(isset($this->request)) {
            return $this->request['screen_name'];
        } else {
            return "none";
        }
    }
    
    protected function isRequestSet() {
        if($this->request) {
            return true;
        } else {
            return false;
        }
    }
    
}