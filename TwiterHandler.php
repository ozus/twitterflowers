<?php

require_once('AccessTokens.php');
require_once('Parser/Parser.php');
require_once('TwitterApiExchange.php');
require_once('HandleTwitterApi.php');
require_once('classes/Loger.php');
require_once('classes/FileSystemInteractions.php');

$token = new AccessTokens();
$parser = new Parser();
$handler = new HandleTwitterApi();
$loger = new Loger();
$fileSystemInteractor = new FileSystemInteractions(new Parser());

set_time_limit(170);

$loger->logTwitterHandle("scheduler started");

$avelable = $handler->checkAvelableTokens();

if(!$avelable) {
	$loger->logTwitterHandle("All connections reached rate limit");
	die("All conections reached rate limit");
}

$j = 0;
$tokensArraySize = $token->getTokenArrayCount();
$limit = $handler->returnMaxLimit();
$responses = array();
do {  
	
	$fileSystemInteractor->checkIfBeginingAndRewind();
	
	$cursor = $handler->getCurrentCursor();
	
	$isTokenAvelable = $handler->checkAvelableTokens();
	if(!$isTokenAvelable) {
		break;
	}
	$i = $token->getKey();

	$handler->buildSettingsArrayAndInstantiate();
	$newCursor = $handler->peroformGetRequest($cursor, $responses);
	
	//for following feed in single action from browser
	echo $newCursor;
	echo "<br><br><br>";
	
	if($newCursor != 0) {
		$handler->updateCursor(strval($newCursor));
	} else {
		$handler->updateCursor("-1");
		$token->setKey(0);
	}
	
	//for following feed in single action from browser
	print_r($responses);
	echo "<br><br>";
	echo $i;
	echo  "<br><br><br><br>";
    	
	if($i > $tokensArraySize -1) {
		$token->setKey(0);
	}
	
	if($newCursor == 0) {
		break;
	}
	
	$j++;
	
}
while ( $j <= (3*$limit) - 1 );

$fileSystemInteractor->updateUserListWithFollower($responses);
