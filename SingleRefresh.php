<?php

include_once 'Parser/Parser.php';
include_once 'HandleTwitterApi.php';
include_once 'classes/FileSystemInteractions.php';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
    $parser = new Parser();
    $handler = new HandleTwitterApi();
    $fileSystemInteractor = new FileSystemInteractions(new Parser());
    
    $handler->buildSingleRefreshSettingsArrayAndInstantiate();
    $result = $handler->performGetRequestForSingleRefresh();
    
    if($result) {
    	echo "<p><b>Synchronization was successfull</b></p>";
    	 
    	$names = array();
    	foreach($result->users as $user) {
    		array_push($names, $user->screen_name);
    		echo $user->screen_name . "<br>";
    	}
    	 
    	 
    	$fileSystemInteractor->updateListOfUsersFromManualRefresh($names, $_POST);
    	 
    } else {
    	echo "Could not synchronize";
    }
}