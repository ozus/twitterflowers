<?php

require_once('../../AccessTokens.php');
require_once('../../Parser/Parser.php');
require_once('../../TwitterApiExchange.php');
require_once('../../classes/FileSystemInteractions.php');
require_once('../../classes/Loger.php');
require_once('../../HandleTwitterApi.php');

/*
 * Logic for testing important parts of aplication is here, there are 3 tests all together.
 * 
 * 
 */

class Tester {
	
	protected $token;
	protected $parser;
	protected $fileSystemInteractor;
	protected $loger;
	protected $twitterHandler;
	
	public function __construct() {
		$this->token = new AccessTokens();
		$this->parser = new Parser();
		$this->loger = new Loger();
		$this->fileSystemInteractor = new FileSystemInteractions(new Parser());
		$this->twitterHandler = new HandleTwitterApi();
	}
	
	
	public function testConnectionsAvelabilitiy() {
		$currentKey = $this->token->getKey();
		$tokenArraySize = $this->token->getTokenArrayCount();
		$connectionStatuses = array();
		
		for($i = 0; $i < $tokenArraySize; $i++) {
			$this->token->setKey($i);
			$this->twitterHandler->buildSettingsArrayAndInstantiate();
			$limit = $this->twitterHandler->checkLimit();
			if($limit['resources']['followers']['/followers/list']['remaining'] > 0) {
				$connectionStatuses['multi'][$i] = "available";
			} else {
				$connectionStatuses['multi'][$i] = "not available";
			}
		}
		
		$this->token->setKey($currentKey);
		
		$this->twitterHandler->buildSingleRefreshSettingsArrayAndInstantiate();
		$limit = $this->twitterHandler->checkLimit();
		if($limit['resources']['followers']['/followers/list']['remaining'] > 0) {
			$connectionStatuses['single'] = "available";
		} else {
			$connectionStatuses['single'] = "not available";
		}
		
		return $connectionStatuses;
	}
	
	public function testConnections() {
		$currentKey = $this->token->getKey();
		$tokenArraySize = $this->token->getTokenArrayCount();
		$connectionStatuses = array();
		
		for($i = 0; $i < $tokenArraySize; $i++) {
			$this->token->setKey($i);
			$this->twitterHandler->buildSettingsArrayAndInstantiate();
			$response = $this->twitterHandler->performTestGetRequest();
			$connectionStatuses['multi'][$i] = $response;
		}
		
		$this->token->setKey($currentKey);
		
		$this->twitterHandler->buildSingleRefreshSettingsArrayAndInstantiate();
		$response = $this->twitterHandler->performTestGetRequest();
		$connectionStatuses['single'] = $response; 
		
		return $connectionStatuses;
	}
	
	
	public function checkNecessaryLists() {
		$results = array();
		$baseDirectory = $_SERVER['DOCUMENT_ROOT'] . "/TwitterFlowers";
		
		if(!is_dir($baseDirectory . "/list")) {
			array_push($results, "Directory wich should contain filesystem, dose not exist");
		} else {
			$lists = scandir($baseDirectory . "/list");
			
			if(!in_array('currentCursot.txt', $lists)) {
				array_push($results, "File that contains current cursor (currentCursot.tx) is missing");
			} else {
				if(!is_readable($baseDirectory . "/list" . "/currentCursot.txt")) {
					array_push($results, "File currentCursot.txt is not readable");
				}
				if(!is_writable($baseDirectory . "/list" . "/currentCursot.txt")) {
					array_push($results, "File currentCursot.txt  is not writable");
				}
			}
			
			if(!in_array('CurrentKey.txt', $lists)) {
				array_push($results, "File that contains current key for iteration (CurrentKey.txt) is missing");
			} else {
				if(!is_readable($baseDirectory . "/list" . "/CurrentKey.txt")) {
					array_push($results, "File CurrentKey.txt is not readable");
				}
				if(!is_writable($baseDirectory . "/list" . '/CurrentKey.txt')) {
					array_push($results, "File CurrentKey.txt is not writable");
				}
			}
			
			if(!in_array('ListOfUsers.txt', $lists)) {
				array_push($results, "File that contains list of users for compareson (ListOfUsers.txt) is missing");
			} else {
				if(!is_readable($baseDirectory . "/list" . '/ListOfUsers.txt')) {
					array_push($results, "File ListOfUsers.txt is not readable");
				}
				if(!is_writable($baseDirectory . "/list" . '/ListOfUsers.txt')) {
					array_push($results, "File ListOfUsers.txt is not writable");
				}
			}
			
			if(!in_array('possibleSearches.txt', $lists)) {
				array_push($results, "File that contains list of posible search targets (possibleSearches.txt) is missing");
			} else {
				if(!is_readable($baseDirectory . "/list" . '/possibleSearches.txt')) {
					array_push($results, "File possibleSearches.txt is not readable");
				}
				if(!is_writable($baseDirectory . "/list" . '/possibleSearches.txt')) {
					array_push($results, "File possibleSearches.txt is not writable");
				}
			}
			
			if(!in_array('ToSearch.txt', $lists)) {
				array_push($results, "File that contains current search target (ToSearch.txt) is missing");
			} else {
				if(!is_readable($baseDirectory . "/list" . '/ToSearch.txt')) {
					array_push($results, "File ToSearch.txt is not readable");
				}
				if(!is_writable($baseDirectory . "/list" . '/ToSearch.txt')) {
					array_push($results, "File ToSearch.txt is not writable");
				}
			}
			
		}
		
		if(empty($results)) {
			return array("Everything is ok with filesystem");
		} else {
			return $results;
		}
		
		
	}
	
	
	
}








