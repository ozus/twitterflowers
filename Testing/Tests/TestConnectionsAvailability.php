<?php

require_once '../Tester.php';
include_once '../../classes/Loger.php';

$tester = new Tester();
$loger = new Loger();

$response = $tester->testConnectionsAvelabilitiy();

foreach($response as $key => $status) {
	if(is_array($status)) {
		foreach($status as $key => $condition) {
			$loger->logTestResults("Connection no. " . $key . " " . $condition);
			echo "Connection no. " . $key . " " . $condition . "<br>";
		}
	} else {
		$loger->logTestResults("Single connection " . $status);
		echo "Single connection " . $status . "<br>";
	}
}