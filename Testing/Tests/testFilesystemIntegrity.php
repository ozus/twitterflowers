<?php

require_once '../Tester.php';
include_once '../../classes/Loger.php';

$tester = new Tester();
$loger = new Loger();

$responses = $tester->checkNecessaryLists();

foreach($responses as $response) {
	$loger->logTestResults($response);
	echo $response . "<br>";
}