<?php

require_once '../Tester.php';
include_once '../../classes/Loger.php';

$tester = new Tester();
$loger = new Loger();

$response1 = $tester->testConnectionsAvelabilitiy();

foreach($response1 as $key => $status) {
	if(is_array($status)) {
		foreach($status as $key => $condition) {
			$loger->logTestResults("Connection no. " . $key . " " . $condition);
		}
	} else {
		$loger->logTestResults("Single connection " . $status);
	}
}

$response2 = $tester->testConnections();

foreach($response2 as $key => $status) {
	if(is_array($status)) {
		foreach($status as $key => $condition) {
			if($condition == false) {
				$loger->logTestResults("Connection no. " . $key . " is not working correctly");
			} else {
				$loger->logTestResults("Connection no. " . $key . " working correctly");
			}
		}
	} else {
		if($status == false) {
			$loger->logTestResults("Single connection is not working correctly");
		} else {
			$loger->logTestResults("Single connection is working correctly");
		}
	}
}


$responses3 = $tester->checkNecessaryLists();

foreach($responses3 as $response) {
	$loger->logTestResults($response);
}