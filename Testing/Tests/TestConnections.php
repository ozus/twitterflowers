<?php

require_once '../Tester.php';
include_once '../../classes/Loger.php';

$tester = new Tester();
$loger = new Loger();

$response = $tester->testConnections();

foreach($response as $key => $status) {
	if(is_array($status)) {
		foreach($status as $key => $condition) {
			if($condition == false) {
				$loger->logTestResults("Connection no. " . $key . " is not working correctly");
				echo "Connection no. " . $key . " is not working correctly" . "<br>";
			} else {
				$loger->logTestResults("Connection no. " . $key . " working correctly");
				echo "Connection no. " . $key . " working correctly" . "<br>";
			}
		}
	} else {
		if($status == false) {
			$loger->logTestResults("Single connection is not working correctly");
			echo "Single connection is not working correctly" . "<br>";
		} else {
			$loger->logTestResults("Single connection is working correctly");
			echo "Single connection is working correctly" . "<br>";
		}
	}
}