<?php

require_once('AccessTokens.php');
require_once('Parser/Parser.php');
require_once('TwitterApiExchange.php');
require_once('classes/FileSystemInteractions.php');
require_once('classes/Loger.php');


/*
 * Handles all traffic between twitter and this app.
 * 
 */

class HandleTwitterApi {
	
	const LIMIT_URL = "https://api.twitter.com/1.1/application/rate_limit_status.json";
	const FOLLOWERS_IDS_URL = "https://api.twitter.com/1.1/followers/ids.json";
	const FOLLOWERS_LIST_URL = "https://api.twitter.com/1.1/followers/list.json";
	const USER_SHOW_LONK = "https://api.twitter.com/1.1/users/show.json";
	
	protected $tokens;
	protected $parser;
	protected $apiExchange;
	protected $fileSystemInteractor;
	protected $loger;
	
	public function __construct() {
		$this->tokens = new AccessTokens();
		$this->parser = new Parser();
		$this->fileSystemInteractor = new FileSystemInteractions(new Parser());
		$this->loger = new Loger();
	}
	
	public function buildSettingsArrayAndInstantiate() {
		$array = $this->tokens->returnTokenArray();
		
		if(!is_array($array) || !count($array) > 0) {
			$this->handleErros("Could not make settings array");
		}
		
		$settings = array(
			'oauth_access_token' => $array['access_token'],
			'oauth_access_token_secret' => $array['access_token_secret'],
			'consumer_key' => $array['consumer_key'],
			'consumer_secret' => $array['consumer_secret']
		);
		
		$this->apiExchange = new TwitterAPIExchange($settings);
		
		if(!isset($this->apiExchange)) {
			$this->handleErros("Could not instantiate twitter exchange object");
		}
	}
	
	public function buildSingleRefreshSettingsArrayAndInstantiate() {
	    $array = $this->tokens->returnSingleTokenArray();
	    
	    if(!is_array($array) || !count($array) > 0) {
	    	$this->handleErros("Could not make settings array");
	    }
	    
	    $settings = array(
	        'oauth_access_token' => $array['access_token'],
	        'oauth_access_token_secret' => $array['access_token_secret'],
	        'consumer_key' => $array['consumer_key'],
	        'consumer_secret' => $array['consumer_secret']
	    );
	    $this->apiExchange = new TwitterAPIExchange($settings);
	    
	    if(!isset($this->apiExchange)) {
	    	$this->handleErros("Could not instantiate twitter exchange object");
	    }
	}
	
	
	public function checkLimit() {
		
		$url = self::LIMIT_URL;
		$requestMethod = "GET";
		$getfield = '?resources=followers';
		$encodedLimit = $this->apiExchange->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
		$limits = json_decode($encodedLimit, true);
		
		if(!$limits) {
			$this->handleErros("No limits returned");
		}
		
		return $limits;
		
	}
	
	
	public function returnMaxLimit() {
		$this->buildSingleRefreshSettingsArrayAndInstantiate();
		$url = self::LIMIT_URL;
		$requestMethod = "GET";
		$getfield = '?resources=followers';
		$encodedLimit = $this->apiExchange->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
		$limits = json_decode($encodedLimit, true);
	
		if(!$limits) {
			$this->handleErros("No limits returned for max check");
			die("No limits returned for max check");
		}
	
		return $limits['resources']['followers']['/followers/list']['limit'];
	}
	
	
	public function peroformGetRequest($cursor, & $responses) {
		
		$url = self::FOLLOWERS_LIST_URL;
		$requestMethod = "GET";
		$this->parser->composeGetString();
		$getfield = $this->parser->getResult();
		$getfield .= "&cursor=" . $cursor;
		if(!strlen($getfield) > 0) {
			$this->handleErros("Colud not make parameters string for get request");
		}
		$this->loger->logTwitterHandle($getfield);
		$response = $this->apiExchange->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
		$this->loger->logTwitterHandle("Response returned for get request");
		$plainResponse = json_decode($response);
		if(!$plainResponse) {
			$this->handleErros("No response returned form the feed for get request");
		}
		$cursor = $plainResponse->next_cursor_str;
		
		foreach($plainResponse->users as $user) {
			array_push($responses, $user->screen_name);
		}
		
		return $cursor;
		
	}
	
	public function performGetRequestForSingleRefresh() {
	    $url = self::FOLLOWERS_LIST_URL;
	    $requestMethod = "GET";
	    $this->parser->composeGetString();
	    $getfield = $this->parser->getResult();
	    if(!strlen($getfield) > 0) {
	    	$this->handleErros("Colud not make parameters string for single get request");
	    }
	    $this->loger->logTwitterHandle($getfield . " single");
	    $response = $this->apiExchange->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
	    $this->loger->logTwitterHandle("Response returned for single request");
	    $plainResponse = json_decode($response);
	    if(!$plainResponse) {
	    	$this->handleErros("No response returned form the feed for single get request");
	    }
	    return $plainResponse;
	}
	
	public function performTestGetRequest() {
		
		$url = self::FOLLOWERS_LIST_URL;
		$requestMethod = "GET";
		$getfield = "?screen_name=userWithNameThatApsolutlyNobodyCanHave78956432&skip_status=1&count=1";
		if(!strlen($getfield) > 0) {
			$this->handleErros("Colud not make parameters string for test get request");
		}
		$this->loger->logTwitterHandle($getfield . " test");
		$response = $this->apiExchange->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
		$this->loger->logTwitterHandle("Response returned for test request");
		$plainResponse = json_decode($response);
		if($plainResponse) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public function returnFirstAvelableToken() {
		$tokenNum = $this->tokens->getTokenArrayCount();
		$currentKey = $this->tokens->getKey();
		for($i = 0; $i < $tokenNum; $i++) {
			$this->tokens->setKey($i);
			$this->buildSettingsArrayAndInstantiate();
			$limit = $this->checkLimit();
			if($limit['resources']['followers']['/followers/list']['remaining'] > 0) {
				$this->loger->logTwitterHandle("Avelable token with array key " . $i);
				return $i;
			}
		}
		$this->tokens->setKey($currentKey);
		$this->loger->logTwitterHandle("No avelable tokens");
		return false;
	}
	
	
	public function checkAvelableTokens() {
		$responses = array();
		$lastKey = 0;
		$returnStatus = 0;
		$tokenNum = $this->tokens->getTokenArrayCount();
		for($i = $tokenNum-1; $i >= 0; $i--) {
			$this->tokens->setKey($i);
			$this->buildSettingsArrayAndInstantiate();
			$limit = $this->checkLimit();
			if(!$limit['resources']['followers']['/followers/list']['remaining'] > 0) {
				array_push($responses, false);
			} else {
				array_push($responses, true);
				$lastKey = $i;
				$returnStatus = 1;
			}
		}
		
		$this->tokens->setKey($lastKey);
		if($returnStatus == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public function checkTokensStatus() {
	    $currentKey = $this->tokens->getKey();
	    $tokenArraySize = $this->tokens->getTokenArrayCount();
	    $connectionStatuses = array();
	    for($i = 0; $i < $tokenArraySize; $i++) {
	        $this->tokens->setKey($i);
	        $this->buildSettingsArrayAndInstantiate();
	        $limit = $this->checkLimit();
	        if($limit['resources']['followers']['/followers/list']['remaining'] > 0) {
	            $connectionStatuses[$i] = "available";
	        } else {
	            $connectionStatuses[$i] = "not available";
	        }
	    }
	    $this->tokens->setKey($currentKey);
	    return $connectionStatuses;
	}
	
	public function checkSingleRefreshTokenStatus() {
	    $this->buildSingleRefreshSettingsArrayAndInstantiate();
	    $limit = $this->checkLimit();
	    if($limit['resources']['followers']['/followers/list']['remaining'] > 0) {
	        return "Single token is available";
	    } else {
	        return "Single token is not available";
	    }
	}
	
	public function updateCursor($newCursor) {
		$this->fileSystemInteractor->updateCursorToFile($newCursor);
	}
	
	public function getCurrentCursor() {
		return $this->fileSystemInteractor->getCursorFromFile();
	}
	
	protected function handleErros($message) {
		$this->loger->logFileSystemException($message);
		die($message);
	}
	
	
}




