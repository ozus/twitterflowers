<?php

require_once('Loger.php');

/*
 * Used for storing and retrieving data form local filesystem. It replaces database and database connection.
 * 
 */

class FileSystemInteractions {
    
    protected $root;
    protected $parser;
    protected $loger;
    
    public function __construct(Parser $parser) {
        $this->root = $_SERVER['DOCUMENT_ROOT'] . "/TwitterFlowers";
        $this->parser = $parser;
        $this->loger = new Loger();
    }
    
    //
    public function getCurrentToSearch() {
        $filename = $this->root . "/list/ToSearch.txt";
        if(!file_exists($filename)) {
        	$this->handleErrors($filename);
        }
        if(file_exists($filename)) {
            $fh = fopen($filename, "r");
            if($fh) {
                $line = fgets($fh);
                $this->parser->parseFileOutput($line);
                $line = $this->parser->getResult();
                if(strlen($line) > 0) {
                    return $line;
                }
            }
        }
    }
    
    public function getSearchesArray() {   
        $searches = array();
        $filename = $this->root . "/list/possibleSearches.txt";
        if(!file_exists($filename)) {
        	$this->handleErrors($filename);
        }
        if(file_exists($filename)) {
            $fh = fopen($filename, "r");
            if($fh) {
                while(($line = fgets($fh)) != FALSE){
                    $this->parser->parseFileOutput($line);
                    $line = $this->parser->getResult();
                    if(strlen($line) > 0) {
                        array_push($searches, $line);
                    }
                }
            }
        }
        return $searches;
    }
    
    
    public function updateCurrentTarget($newTarget) {
        $filename = $this->root . "/list/ToSearch.txt";
        if(!file_exists($filename)) {
        	$this->handleErrors($filename);
        }
        if(file_exists($filename)) {
            $fh = fopen($filename, "w");
            if($fh) {
                fwrite($fh, $newTarget);
                fclose($fh);
            }
        }
        $filename = $this->root . "/list/currentCursot.txt";
        if(file_exists($filename)) {
        	$fh = fopen($filename, "w");
        	if($fh) {
        		fwrite($fh, "-1");
        		fclose($fh);
        	}
        }
    }
    
    //
    public function getKeyFormFile() {
    	$filename = $this->root . '/list/CurrentKey.txt';
    	if(!file_exists($filename)) {
    		$this->handleErrors($filename);
    	}
    	if(file_exists($filename)) {
    		$fh = fopen($filename, 'r');
    		if($fh) {
    			$line = fgets($fh);
    			$this->parser->parseFileOutput($line);
    			$line = $this->parser->getResult();
    			$line = intval($line);
    			return $line;
    		}
    	}
    }
    
    
    public function setKeyToFile($newKey){
    	$filename = $this->root . '/list/CurrentKey.txt';
    	if(!file_exists($filename)) {
    		$this->handleErrors($filename);
    	}
    	if(file_exists($filename)) {
    		$fh = fopen($filename, 'w');
    		if($fh) {
    			fwrite($fh, $newKey);
    			fclose($fh);
    		}
    	}
    }
    
    //
    public function updateCursorToFile($newCursor) {
    	$filename = $this->root . '/list/currentCursot.txt';
    	if(!file_exists($filename)) {
    		$this->handleErrors($filename);
    	}
    	if(file_exists($filename)) {
    		$fh = fopen($filename, 'w');
    		if($fh) {
    			fwrite($fh, $newCursor);
    			fclose($fh);
    		}
    	}
    }
    
    
    public function getCursorFromFile() {
    	$filename = $this->root . '/list/currentCursot.txt';
    	if(!file_exists($filename)) {
    		$this->handleErrors($filename);
    	}
    	if(file_exists($filename)) {
    		$fh = fopen($filename, 'r');
    		if($fh) {
    			$line = fgets($fh);
    			$this->parser->parseFileOutput($line);
    			$line = $this->parser->getResult();
    			return $line;
    		}
    	}
    }
    
    public function updateUserListWithFollower($responses, $request=NULL) {
    	$this->loger->logTwitterHandle("Update user list started");
    	$parsedLines = array();
    	if($request) {
    	    $this->parser->setRequest($request);
    	}
    	$screenNameFromRequest = $this->parser->GetScreenName();
    	$currentToSearch = $this->getCurrentToSearch();
    	if(is_dir($this->root . '/list')) {
    		$filename = $this->root . '/list/ListOfUsers.txt';
    		if(!file_exists($filename)) {
    			$this->handleErrors($filename);
    		}
    		if(file_exists($filename)) {
    			$fh = fopen($filename, "r");
    			if($fh) {
    				while(($line = fgets($fh)) !== FALSE) {
    					
    					$this->parser->parseFileOutput($line);
    					$line = $this->parser->getResult();
    					$idNameFollower = explode(',', $line);
    						
    					if(count($idNameFollower) == 3) {
    						unset($idNameFollower[2]);
    					}
    					foreach($responses as $user) {
    						if($user == $idNameFollower[1]) {
    						    if($screenNameFromRequest != "none") {
    						        $idNameFollower[] = $screenNameFromRequest;
    						    } else {
    							     $idNameFollower[] = $currentToSearch;
    							     $idNameFollower[] = 1;
    						    }
    						}
    					}
    					$parsedLine = implode(",", $idNameFollower);
    					$this->loger->logTwitterHandle("User update " . $parsedLine);
    					array_push($parsedLines, $parsedLine);
    				}
    				fclose($fh);
    			}
    		}
    		$output = implode(PHP_EOL, $parsedLines);
    		if(strlen($output) > 0) {
    			file_put_contents($filename, $output);
    		}
    	}
    	
    }
    
    
    public function updateListOfUsersFromManualRefresh($responses, $request) {
    	$this->parser->setRequest($request);
    	$screenNameFromRequest = $this->parser->GetScreenName();
    	$filename = $this->root . '/list/ListOfUsers.txt';
    	$parsedLines = array();
    	if(!file_exists($filename)) {
    		$this->handleErrors($filename);
    	}
    	if(file_exists($filename)) {
    		$fh = fopen($filename, "r");
    		if($fh) {
    			while(($line = fgets($fh)) !== FALSE) {
    				$this->parser->parseFileOutput($line);
    				$line = $this->parser->getResult();
    				$idNameFollower = explode(',', $line);
    
    				foreach($responses as $user) {
    					if($user == $idNameFollower[1]) {
    						if(count($idNameFollower) == 3) {
    							unset($idNameFollower[2]);
    						}
    						if(count($idNameFollower) == 2) {
    							if($screenNameFromRequest != "none") {
    								$idNameFollower[] = $screenNameFromRequest;
    								$idNameFollower[] = 1;
    							}
    						}
    					}
    				}
    				$parsedLine = implode(",", $idNameFollower);
    				$this->loger->logTwitterHandle("User update single " . $parsedLine);
    				array_push($parsedLines, $parsedLine);
    			}
    			 
    			$output = implode(PHP_EOL, $parsedLines);
    			if(strlen($output) > 0) {
    				file_put_contents($filename, $output);
    			}
    			 
    		}
    	}
    }
    
    
    public function checkIfBeginingAndRewind() {
    	$filename = $this->root . '/list/ListOfUsers.txt';
    	$currentCursor = $this->getCursorFromFile();
    	if(!file_exists($filename)) {
    		$this->handleErrors($filename);
    	}
    	if($currentCursor == -1) {
    		if(file_exists($filename)) {
    			$outputs = array();
    			$fh = fopen($filename, 'r');
    			if($fh) {
    				while(($line = fgets($fh)) !== FALSE) {
    					$this->parser->parseFileOutput($line);
    					$line = $this->parser->getResult();
    					$pices = explode(",", $line);
    					 
    					if(count($pices) == 4) {
    						unset($pices[3]);
    					}
    					$output = implode(",", $pices);
    					array_push($outputs, $output);
    				}
    
    				$toInsert = implode(PHP_EOL, $outputs);
    				file_put_contents($filename, $toInsert);
    			}
    		}
    	}
    }
    
    
    
    protected function handleErrors($filename) {
    	$this->loger->logFileSystemException("File with path " . $filename . " could not be opened, no such file or directory");
    	die("File with path " . $filename . " could not be opened, no such file or directory");
    }
    
}



