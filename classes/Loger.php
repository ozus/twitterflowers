<?php

/*
 * Used for make logs and record data to logs.
 * 
 */

class Loger {
	
	protected $root;
	
	public function __construct() {
		$this->root = $_SERVER['DOCUMENT_ROOT'] . "/TwitterFlowers";
	}
	
	
	public function logTwitterHandle($line) {
		$filename = $this->root . "/Logs/TwitterHandlerLog.log";
		$fh = fopen($filename, "a");
		if($fh) {
			fwrite($fh, "[" . date("Y-m-D H:i:s") . "] " . $line . "\n");
			fclose($fh);
		}
	}
	
	public function logFileSystemException($line) {
		$filename = $this->root . "/Logs/FilesystemErrors.log";
		$fh = fopen($filename, "a");
		if($fh) {
			fwrite($fh, "[" . date("Y-m-D H:i:s") . "] " . $line . "\n");
			fclose($fh);
		}
	}
	
	public function logTwitterException($line) {
		$filename = $this->root . "/Logs/TwitterErrors.log";
		$fh = fopen($filename, "a");
		if($fh) {
			fwrite($fh, "[" . date("Y-m-D H:i:s") . "] " . $line . "\n");
			fclose($fh);
		}
	}
	
	public function logAccessTokensException($line) {
		$filename = $this->root . "/Logs/TokenErrors.log";
		$fh = fopen($filename, "a");
		if($fh) {
			fwrite($fh, "[" . date("Y-m-D H:i:s") . "] " . $line . "\n");
			fclose($fh);
		}
	}
	
	public function logTestResults($line) {
		$filename = $this->root . "/Logs/TestResults.log";
		$this->logToFile($filename, $line);
	}
	
	protected function logToFile($filename, $line) {
		$fh = fopen($filename, "a");
		if($fh) {
			fwrite($fh, "[" . date("Y-m-D H:i:s") . "] " . $line . "\n");
			fclose($fh);
		}
	}	
	
	
}
