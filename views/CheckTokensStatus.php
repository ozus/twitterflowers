<?php

include_once '../HandleTwitterApi.php';

$handler = new HandleTwitterApi();
$statuses = $handler->checkTokensStatus();

foreach($statuses as $key => $status) {
    echo "Token " . $key . " has status " . $status . "<br>";
}