<?php 
    include_once('../classes/FileSystemInteractions.php'); 
    include_once('../Parser/Parser.php');
?>
<html>
	<head>
		<title>Custom sync</title>
		<meta charset="utf-8">
	</head>
	<body>
		<?php       
            $searches = (new FileSystemInteractions(new Parser()))->getSearchesArray();
        ?>
        <h2>Manual Synchronization</h2>
        <h4>Please note that you can only refresh from list of 200 followers with this request!</h4>
		<form action="../SingleRefresh.php" method="POST" id="manualSyncForm">
			<label for="searchesSelect">Select user</label><br>
			<select id="searchesSelect" name="screen_name">
				<?php foreach($searches as $searches) {?>
					<option value="<?php echo htmlspecialchars($searches); ?>"><?php echo htmlspecialchars($searches); ?></option>
				<?php } ?>
			</select><br><br>
			<input type="submit" value="Synchronize">
		</form>
		
	</body>
</html>

