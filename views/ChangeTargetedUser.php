<?php
    include_once("../classes/FileSystemInteractions.php");
    include_once '../Parser/Parser.php';
?>
<html>
	<head>
		<meta charset="utf-8">
		<title>Change target user</title>
	</head>
	<body>
		<?php 
		    $fileSystemInteractor = new FileSystemInteractions(new Parser());
        	if($_SERVER['REQUEST_METHOD'] == 'POST') {
        		   $fileSystemInteractor->updateCurrentTarget($_POST['new_target']);
        	}
		      $currnetTarget = $fileSystemInteractor->getCurrentToSearch();
		      $posibleTargets = $fileSystemInteractor->getSearchesArray();
		?>
		<h2>Update targeted user</h2>
		Currnet target:&nbsp; <?php echo htmlspecialchars($currnetTarget); ?><br><br>
		<form action="" method="POST" id="updateTargetForm">
			<label for="newTagetsSelect">Choose new target</label><br>
			<select id="newTagetsSelect" name="new_target">
				<?php foreach($posibleTargets as $posibleTarget) {?>
					<option value="<?php echo htmlspecialchars($posibleTarget); ?>"><?php echo htmlspecialchars($posibleTarget); ?></option>
				<?php } ?>
			</select><br><br>
			<input type="submit" value="Update">
		</form>
	</body>
</html>