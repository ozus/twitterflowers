<?php

require_once('Parser/Parser.php');
require_once('classes/FileSystemInteractions.php');
include_once 'classes/Loger.php';

/*
 * This class contains token arrays and holds all logic to work with tokens inside of this application.
 * 
 * 
 */


class AccessTokens {
    
    protected $key;
    protected $max;
    protected $fileSystemInteractor;
    protected $loger;
    static protected $filename = 'list/CurrentKey.txt';
    
    //aplicationm will atempt to connect to different twitter app if rate limit is excited, this array contains tokkens for those apps
    static protected $tokenArray = array(
        [
            'access_token' => '967499526754983936-tLVxXzsG33IZBZ9t3WtTZ7cpTuVAJ79',
            'access_token_secret' => '0ymDWgbSgoCGJWhlM7qnfW13UX5jfHTUxJWZHMbNwFLot',
            'consumer_key' => 'Ce1RBN69NNlQ949bmXs2aX9gR',
            'consumer_secret' => 'oxqbocfTnyvVWKY9WWv8n1YfbC7tKvrA9ulIvxJKHS7nH3kRtt',
        ],
        
        [
            'access_token' => '967499526754983936-htocDL4OZVr6JvcZc3jocmlFhu2ezpr',
            'access_token_secret' => 'yHfNNg9rptVsWV1TT1mdvC7Jwp2E7qgupbCwEXXrm9SRY',
            'consumer_key' => 'AhGdAgr7Z4F4eiU13sg9xXGSW',
            'consumer_secret' => 's7Fq5L9CMNB4k9igWkZq0obH4vnZgFH0Vb9tnHaTQUYKZeUImL',
        ],
        
        [
            'access_token' => '967499526754983936-wa6aFns3RXpCK9nLslRuLaGboa6Uj7T',
            'access_token_secret' => 'TxAZn66l9U3dAVS76jY25FGAtXq6Y77xjn4NaVFyj5gIx',
            'consumer_key' => 'GsfjsiN4xmylpZiB3wswgG8R2',
            'consumer_secret' => 'VpdLorBY2898WDvh1lIFO8J5uCUtRzTZbxPgMeyqdFjrZ4KIyg',
        ],
    		
    	[
    		'access_token' => '967499526754983936-2qs9chrYq6jCAbEBHTyormcs3kVJ9vO',
    		'access_token_secret' => 'jYf3XEu3ewaK9ze8NBHk0X9RXGwQ9mIKomlFnAgvpGtSa',
    		'consumer_key' => 'hdPOcjvKZjtTNKrNGFbbfA9aA',
    		'consumer_secret' => 'xKzCs2vET4H3LoHE3UR3CHhbHCTKZsipYdHb4hBdIgHrjA9KIs',
    	],
    		
    	[
    		'access_token' => '967499526754983936-KEuVfelS5XBLZtWVbm2pXRadUIjRh5k',
    		'access_token_secret' => 'RCZxjlhovhLcD1jXDdzpbLO3QmRZMONhvugMg8oKofEfu',
    		'consumer_key' => 'cXtwFDRn5HXhoiO7Ywi5IkADw',
    		'consumer_secret' => 'UM1e5ylTTxqMCCjqxTUUdVBSnMm9R4x1Ex8yEgELOgSSfa5v5L',
    	],
        
    );
    
    static protected $singleRefreshToken = array(
        'access_token' => '967499526754983936-Qug861Fm6zO7gtN3DdkJm9AJjjn4fFn',
        'access_token_secret' => '9ebIYcuczTUSgN9l6dV4f35C1TLTv4jNamO3KMNa3V1xr',
        'consumer_key' => 'qpscYpAg8HjP3SnTCRdSYhzXh',
        'consumer_secret' => '1GnLNOUZYRvCWYtLV9qxD8TXgjwCazwRxUlykSbMCcC4I4bWMT',
    );
    
    static protected $testToken = array(
    	'access_token' => '967499526754983936-YP9l7XB4nMmWYodzd0W7iNXhfwi23tR',
    	'access_token_secret' => '7lxieSvHuzeswj12l3x4K5lTuWhIwgiBDYMWeLbBU77XU',
    	'consumer_key' => 'KUHqOzk6iynJhJLpwyN9pWuyO',
    	'consumer_secret' => 'Uc675G2gYrBXkostIYCDHg8e2snXdqIrf2GxKOCxmjQCUxOeOa',
    );
    
    public function __construct() {
    	if(!is_array(self::$tokenArray) || !count(self::$tokenArray) >0 || !isset(self::$tokenArray)) {
    		$this->handleErrors("Tokens array is not configured propertly");
    	}
    	if(!is_array(self::$singleRefreshToken) || !count(self::$singleRefreshToken) >0 || !isset(self::$singleRefreshToken)) {
    		$this->handleErrors("Single token array is not configured propertly");
    	}
        $this->max = count(self::$tokenArray);
        $this->fileSystemInteractor = new FileSystemInteractions(new Parser());
        $this->loger = new Loger();
        if(file_exists(self::$filename)) {
            $fh = fopen(self::$filename, 'r');
            if($fh) {
                $key = fgets($fh);
                
                $parser = new Parser();
                $parser->parseFileOutput($key);
                $key = $parser->getResult();
                
                $key = intval($key);
                if($key > ($this->max - 1)) {
                    $key = 0;
                }
                $this->key = $key;
                fclose($fh);
            }
        } 
    }
    
    
    public function setKey($newKey) {
    	$this->fileSystemInteractor->setKeyToFile($newKey);
    	$this->key = $newKey;
    	

    }
    
    public  function getKey() {
    	$key = $this->fileSystemInteractor->getKeyFormFile();
    	return $key;
    }
    
    public function getTokenArrayCount() {
        return $this->max;
    }
    
    public function returnTokenArray() {
        return self::$tokenArray[$this->getKey()];    
    }
    
    public function returnSingleTokenArray() {
        return self::$singleRefreshToken;
    }
    
    public function returnTestTokenArray() {
    	return self::$testToken;
    }
    
    protected function handleErrors($message) {
    	$this->loger->logAccessTokensException($message);
    	die($message);
    }
       
}

